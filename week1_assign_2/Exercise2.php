
<?php


	$log = '/var/log/apache2/access.log';		//this is where the logs are


	$cont =  file_get_contents("". $log );		//get the file content to parse

	$rows = explode("\n", $cont);		//split line
	$words = array();
	foreach($rows as $row)		//initialize rows with resultant arrays
	{
		$temp = explode(" ", $row);	//now split on space
		foreach($temp as $word)
			$words[] = $word;	//words initialized with splitted arrays 
	}

	foreach($words as $test)
	{
		//check tokens and see if any will match
		$flag = ereg("[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+|-|[0-9]+.[A-Za-z]+.[0-9]+:[0-9]+:[0-9]+:[0-9]+|GET|POST|DELETE|[A-Za-z0-9]+.php|[A-Za-z0-9]+.png|[A-Za-z0-9]+.ico|[A-Za-z0-9]+.html|TTP.1.1", $test);
		if ($flag)	//there's a match
		{
			if($test == '"-"')	//new line?
				echo "\n";
			else
			{
				if(ereg("0-9]+.[A-Za-z]+.[0-9]+:[0-9]+:[0-9]+:[0-9]+",$test)	//is it a date?
				{

					$dates = explode(":", $test);	//take out unrelated tokens
					for($dates as $date)
					{
						$newDate = DateTime::createFromFormat('Y-m-d',$date));	//change format
						echo $newDate->format('l-F d Y ')."\t";	//print
					}
				}
				else
					echo $test."\t";	//still same line

			}

		}

	}
?>


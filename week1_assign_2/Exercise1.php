#! /bin/bash/php
<?php


	function listFiles($input_dir)		//function that displays the list of files having input_dir in their directory
	{
		$main_list = scandir($input_dir);	//scandir is a function that returns an array of files within a directory directly
		foreach($main_list as $temp_var)	//for every entry in the list temp_var = name of the file
		{
			if($temp_var != '.' && $temp_var != '..')	//check if those files aren't '.' and '..' -> actual files
			{
				echo $temp_var."\n";		//print those files within directory

				if(is_dir($input_dir.'/'.$temp_var))		//check if <directory>/<current file> exists -> folder?
					 listFiles($input_dir.'/'.$temp_var);		//redo all the process again if it's a folder
			}
		}
	}


//	$directory = fgets(STDIN);

	$directory = $argv[2];	//initialize the main directory
	echo "files within $directory:\n";
	listFiles($directory);		//initialize function's input

?>

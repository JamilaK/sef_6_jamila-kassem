SELECT 
    CONCAT(customer.first_name, customer.last_name) AS NAME,
    COUNT(*) AS TOTAL,
    customer.customer_id AS ID
FROM
    sakila.customer,
    sakila.rental
WHERE
    customer.customer_id = rental.customer_id
        AND YEAR(rental.rental_date) = 2005
GROUP BY ID
ORDER BY TOTAL DESC
LIMIT 3;


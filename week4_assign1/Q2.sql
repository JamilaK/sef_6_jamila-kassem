SELECT 
    NAME
FROM
    (SELECT 
        language.name AS NAME,
            COUNT(*) AS total,
            language.language_id
    FROM
        sakila.language, sakila.film
    WHERE
        language.language_id = film.language_id
            AND film.release_year = 2006
    GROUP BY language_id
    ORDER BY total DESC
    LIMIT 3) AS TEMP;
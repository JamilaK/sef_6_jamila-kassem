SELECT 
    address.address2
FROM
    sakila.address
WHERE
    address.address2 != ''
order by address.address2 ASC;
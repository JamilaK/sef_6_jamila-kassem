SELECT 
    YEAR(payment.payment_date) AS yr,
    MONTH(payment.payment_date) AS mnth,
    AVG(payment.amount)
FROM
    sakila.payment
GROUP BY yr , mnth;
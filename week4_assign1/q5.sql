SELECT 
    actor.first_name, actor.last_name, film.release_year
FROM
    sakila.film,
    sakila.actor,
    sakila.film_actor
WHERE
    film.film_id = film_actor.film_id
        AND actor.actor_id = film_actor.actor_id
        AND (film.title LIKE '%SHARK%'
        OR film.title LIKE '%CROCODILE%')
ORDER BY actor.last_name DESC;

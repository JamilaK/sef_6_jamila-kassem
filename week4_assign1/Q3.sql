SELECT 
    cntry
FROM
    (SELECT 
        country.country AS cntry,
            COUNT(*) AS TOTAL,
            customer.customer_id AS ID
    FROM
        sakila.country, sakila.city, sakila.address, sakila.customer
    WHERE
        country.country_id = city.country_id
            AND city.city_id = address.city_id
            AND address.address_id = customer.address_id
    GROUP BY ID
    ORDER BY TOTAL DESC
    LIMIT 3) AS TEMP;

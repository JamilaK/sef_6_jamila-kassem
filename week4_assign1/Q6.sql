
SELECT SQL_CALC_FOUND_ROWS
    *
FROM
    (SELECT 
        NAME
    FROM
        (SELECT 
        category.name AS NAME,
            COUNT(*) AS TOTAL,
            category.category_id AS ID
    FROM
        sakila.category, sakila.film, sakila.film_category
    WHERE
        category.category_id = film_category.category_id
            AND film.film_id = film_category.film_id
    GROUP BY ID) AS TABLE1
    WHERE
        (TOTAL >= 50 AND TOTAL <= 65)) AS TEST 
UNION SELECT 
    NAME
FROM
        (SELECT 
        category.name AS NAME,
            COUNT(*) AS TOTAL,
            category.category_id AS ID
    FROM
        sakila.category, sakila.film, sakila.film_category
    WHERE
        category.category_id = film_category.category_id
            AND film.film_id = film_category.film_id
    GROUP BY ID) AS TABLE2
WHERE
    (TOTAL < 50 OR TOTAL > 65)
        AND FOUND_ROWS() = 0;

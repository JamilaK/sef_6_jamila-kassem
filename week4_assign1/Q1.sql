SELECT 
    NAME, COUNT(*) AS '# OF MOVIES'
FROM
    (SELECT 
        actor.actor_id AS ID,
            CONCAT(actor.first_name, actor.last_name) AS NAME,
            film.film_id
    FROM
        sakila.actor 
    JOIN sakila.film_actor ON actor.actor_id = film_actor.actor_id
    JOIN sakila.film ON film_actor.film_id = film.film_id) AS TEMP
GROUP BY ID;
#!usr/bin/php

<?php 

//Main Starts

//Initialize the number of games that will be played
$NumberOfGames = 5;

//Loop over the number of games
for ($Counter = 1; $Counter <= $NumberOfGames; $Counter++)	{

	echo "Game Number {$Counter} \n";
	//Generate new game
	$Game = new GameGenerator;
	//Set the numbers
	$List = $Game -> SetGenerator	();
	//Solve it
	$Game -> Gamesolver	($List);

}

//Main Ends

//A class with objects and methods
class GameGenerator	{

	//Public objects that will be used
	public $Numbers;
	public $Result;
	public $Target;
	public $Unfinished_Result;
	public $Used_Operations;
	public $NumberOfOperators;
	public $Number_used;
	
	//First method that will generate the random set 
	function SetGenerator	()	{

		$Numbers = array();
		$NumberCount_1 = rand(1, 4);
		$NumberCount_2 = 6 - $NumberCount_1;		

		for ($LoopCounter = 0; $LoopCounter < $NumberCount_1; $LoopCounter++) {
			
			$Twodigits_List = array ( 25, 50, 75, 100 );
			$NumberIndex = rand(0, 3);
			array_push( $Numbers, $Twodigits_List[$NumberIndex] );

		}
	
		for ($LoopCounter = 0; $LoopCounter < $NumberCount_2; $LoopCounter++)	{
			
			$Singledigit_List = array ( 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10 );
			$NumberIndex = rand(0, 19);
			array_push( $Numbers, $Singledigit_List[$NumberIndex] );

		}

		return ($Numbers);
 
	}

	//the method that will be solving the game
	function  Gamesolver	($NumberList) {

		$FalseStack = new $Stack;
		
		$Target = rand(101, 999);
		$OperationSet = array("+", "-", "*", "/");
		$Number_used = rand(2, 6);
		$Unfinished_Result = array();
		$Used_Operations = array();
		$NumberOfOperators = $Number_used - 1;

		for ($L_Couter = 0; $L_Couter < $Number_used; $L_Couter ++) { 

			$NumIndex = rand(0, 5);
			array_push($Unfinished_Result, $NumberList[$NumIndex]);

		}

		for ($O_Counter = 0; $O_Counter < $NumberOfOperators ; $O_Counter++) { 
				
				$OpIndex = rand(0, 3);
				array_push($Used_Operations, $OperationSet[$OpIndex]);

		}

		$Flag;
		$Equation = $Unfinished_Result;
		array_push($Equation, $Used_Operations);

		while ($FalseStack-> valid()) {
			
			if ($FalseStack-> current() == $Equation) {

				$Flag = 0;
				return false;
			}

			$FalseStack-> next();
		}

		$calculate;

		if ($Flag != 0) {
			
			//calculate
			for ($F_Loop = 0; $F_Loop < $Number_used; $F_Loop = $F_Loop + 2) { 
				
				for ($S_Loop = 1; $S_Loop < $NumberOfOperators; $S_Loop + 2) { 
					
					$Calculate = $Unfinished_Result[$F_Loop].$Used_Operations[S_Loop];
				}

			}

		return $Calculate;

		}

		//check
		if ($Target - $Calculate < 5) {
			
			return $this-> GameValidate($Calculate);
		}

		//if false add to stack
		else {

			$stack-> incorrectStack($Equation);
		}
		
		

	}

	function  GameValidate	($Variable)	{

		//exact
		if ($Target - $Variable == 0) {
			
		
			$Accuracy = 1; 
			$difference = 0;
		}

		//approximate
		else {

			$difference = $Target - $Variable;
			$Accuracy = 1; 

		}

		return $this-> GameOutput($Accuracy, $difference);

	}

	//output method
	function  GameOutput	($Accuracy_Value, $difference_Value) {

		$Final_Number_Set = $Unfinished_Result;
		$Final_Operations = $Used_Operations;
		$Final_Result = array();
		
		//check if operations ultered
		for ($Check_Count = 0; $Check_Count < $NumberOfOperators - 1; $Check_Count++) { 
			
			if ($Final_Operations[$Check_Count] == $Final_Operations[$Check_Count + 1] && $Final_Operations[$Check_Count] != $Final_Operations[$Check_Count + 2]) {
				
				$Final_Operations = "(".$Final_Operations[$Check_Count].$Final_Operations[$Check_Count + 1].")";

			}
		}

		echo "{";
		for ($Print_Count = 0; $Print_Count < $Number_used; $Print_Count ++) { 

				echo "".$Final_Number_Set[$Print_Count].",";
			}
		echo "}\n";

		if ($Accuracy_Value = 0) {
			
			echo "Solution [exact]: \n";
		}

		else {

			echo "Solution [+ {$difference_Value} \n";
		}		

		for ($F_Loop = 0; $F_Loop < $Number_used; $F_Loop = $F_Loop + 2) { 
				
			for ($S_Loop = 1; $S_Loop < $NumberOfOperators; $S_Loop + 2) { 
					
				echo $Unfinished_Result[$F_Loop].$Used_Operations[S_Loop]."\n";
				
			}

		}	

	}

}

class Stack {

    private $splstack;

    function __construct(\SplStack $splstack)
    {
        $this->splstack = $splstack;
    }

    public function incorrectStack($Value)
    {

       $this->splstack->push($Value);
       
    }

}




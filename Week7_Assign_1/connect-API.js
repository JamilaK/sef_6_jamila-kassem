function httpGet(URL_Text)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "/summarize?url="+URL_Text, false ); // false for synchronous request
    xmlHttp.send();
    return xmlHttp.responseText;
}
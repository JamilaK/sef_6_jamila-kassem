<?php

require_once("database/MySQLWrap.php");

class ServerController 
{
  public function  Request_Controller($request_method, $request, $query_string) {
    
    //first word is always the table name
    $table_name = $request[0];
    //there's a switch here checking the request method
    switch ($request_method) {
      //if GET
      case 'GET':
        $sql_wrapper = new MySQLWrap();
        $used_variables = $request[1];
        print_r($request);

        if ($query_string) {
          $sql_wrapper->selectQuery($request_method, $table_name, $query_string);
        }
        else {
          $sql_wrapper->select($request_method, $table_name, $used_variables);  
        }  
        break;

      case 'PUT':
        $data_update = $request[1];
        $input = json_decode(file_get_contents('php://input'),true);
        $sql_wrapper->update($request_method, $table_name, $data_update, $input);
      break;

      case 'POST':
       $input = json_decode(file_get_contents('php://input'),true);
       $sql_wrapper->insert($request_method, $table_name, $input);
      break;
    }
  }
}

<?php

class DB_Connect
{
	public function connect()
	{
		require_once ('config.php');
		$dbConnection = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
		return $dbConnection;
	}

	public function close()
	{
		mysql_close();
	}
}
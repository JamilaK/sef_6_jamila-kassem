<?php

class MySQLWrap {

 	public function DBConnection() {

		require_once("DBConnect.php");
		$connect = new DB_Connect();
		$con = $connect->connect();
		return $con;
	}

	public function select($request_method, $table, $conditions) {

		$con = $this->DBConnection();
		//api/index.php/film
		if (!$conditions) {

			$sql = "select * from `$table`";
		}
		//api/index.php/film/film_id={1,2,3,4}
		else {

			$conditions = explode("=", $conditions);
			$keyword = $conditions[0];
			$attributes = explode(',', trim($conditions[1],'{}'));
			$more_conditions = "";
			for ($counter = 0; $counter < count($attributes); $counter++) {

				$attribute = $attributes[$counter];
				if ($more_conditions != "or") {

		      		$more_conditions = "or";
		       		$sql = "select * from `$table` WHERE $keyword = $attribute"; 
		    	}
		    	else {
		      		
		      		$sql .= " or $keyword = $attribute";
		      	}
			}
		}

		$result = mysqli_query($con, $sql);
		$this->display($con, $request_method, $conditions, $result);
		$connect->close();
	}

	public function selectQuery($request_method, $table, $query_string)
	{
		$con = $this->DBConnection();
		//conditions joined with &
		$query_string = explode("&", $query_string);
		$more_conditions = "";
		for ($counter = 0; $counter < count($query_string); $counter++) {
			
			$details_set = explode("=", $query_string[$counter]);
			$keyword = $details_set[0];
			$attributes = $details_set[1];
			switch ($keyword) {
			//api/index.php/film?fields=title,description	
      		case 'fields':
      			echo  "field";
      			$more_conditions=  "fields";
        		$sql = "select $attributes from `$table`";  
        		break;

      		case 'orderby':
      			echo "order";
      			//api/index.php/film?fields=title&orderby=title
      			if ($more_conditions == "fields") {
        			$sql .= " order by $attributes"; 
        			break;
        		}
      			//api/index.php/film?orderby=title
      			else {
      				$sql = "select * from `$table` order by $attributes";
      				break;
      			}

      		//api/index.php/film?fields=title&orderby=title&sort=desc	
      		case 'sort':
      			echo "sort";
        		$sql .= " $attributes"; 
        		break;

      		//api/index.php/film?search=%ace%  
      		case 'search':
        		echo "search";
        		$searchedfield = $details_set[1];
        		$attributes = $details_set[2];
        		$sql = "select * from `$table` where $searchedfield like '$attributes'";   
        		break;
        	}
		}
		
		$result = mysqli_query($con, $sql);
		$this->display($con, $request_method, $query_string, $result);
		$connect->close();
	}

	public function insert($request_method, $table, $conditions)
	{
		$con = $this->DBConnection();
		$set = '';
		//$sql = "INSERT INTO $table VALUES($data[0],$data[1],$data[2],$data[3],$data[4],$data[5],$data[6],$data[7],$data[8],$data[9],$data[10])";
		$sql = "INSERT INTO $table set $set";
		$result = mysqli_query($con, $sql);
		var_dump($result);
		$this->display($con, $request_method, $conditions, $result);
		$connect->close();
	}

	public function display($con, $request_method, $conditions, $result) {
		
		if ($request_method == 'GET') {

			if (mysqli_num_rows($result) == 0) {

				echo "Sorry no results !!";
			}
			else {

				if (!$conditions) {

				 	echo '[';
				}
				for ($counter = 0; $couter < mysqli_num_rows($result); $counter++) {

				  echo ($counter > 0?',':'').json_encode(mysqli_fetch_object($result));
				}
				if (!$conditions) echo ']';
			}
		} 
		elseif ($request_method == 'POST') {
			echo mysqli_insert_id($con);
		} 
		else {
			echo mysqli_affected_rows($con);
		}

	}
}
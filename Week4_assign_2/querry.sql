USE DB;
SELECT 
    patient_name, ID, claim_status
FROM
    DB.Claims,
    (SELECT 
        claim_status, ID
    FROM
        DB.claim_status_code, (SELECT 
        MIN(TOTAL) AS MINIMUM, ID
    FROM
        (SELECT 
        defendant_name, claim_id AS ID, COUNT(*) AS TOTAL
    FROM
        DB.LegalEvents
    GROUP BY defendant_name , ID) AS TEMP
    GROUP BY ID) AS TABLE1
    WHERE
        claim_seq = MINIMUM) AS TABLE2
WHERE
    ID = Claims.claim_id;

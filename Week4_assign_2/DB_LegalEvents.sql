-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: DB
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `LegalEvents`
--

DROP TABLE IF EXISTS `LegalEvents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LegalEvents` (
  `claim_id` int(11) NOT NULL,
  `defendant_name` varchar(45) NOT NULL,
  `claim_status` varchar(45) NOT NULL,
  `change_date` date NOT NULL,
  KEY `claim_id_idx` (`claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LegalEvents`
--

LOCK TABLES `LegalEvents` WRITE;
/*!40000 ALTER TABLE `LegalEvents` DISABLE KEYS */;
INSERT INTO `LegalEvents` VALUES (2,'Radwan Sameh','AP','2016-01-01'),(2,'Radwan Sameh','OR','2016-02-01'),(2,'Paul Syoufi','AP','2016-01-01'),(3,'Issam Awwad','AP','2016-01-01'),(1,'Jean Skaff','AP','2016-01-01'),(1,'Jean Skaff','OR','2016-02-02'),(1,'Jean Skaff','SF','2016-03-01'),(1,'Jean Skaff','CL','2016-04-01'),(1,'Radwan Sameh','AP','2016-01-01'),(1,'Radwan Sameh','OR','2016-02-02'),(1,'Radwan Sameh','SF','2016-03-01'),(1,'Elie Meouchi','AP','2016-01-01'),(1,'Elie Meouchi','OR','2016-02-02');
/*!40000 ALTER TABLE `LegalEvents` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-15 13:52:33
